# little_penguin

## Usefull links :

- 01:
	https://kernelnewbies.org/FAQ/LinuxKernelDebug101

- 04 :
	https://github.com/bashrc/LKMPG/blob/master/4.15.2/LKMPG-4.15.2.org
	https://debian-handbook.info/browse/stable/sect.hotplug.html
	https://www.kernel.org/doc/html/v4.12/driver-api/usb/writing_usb_driver.html
	https://static.lwn.net/images/pdf/LDD3/ch14.pdf
	http://www.reactivated.net/writing_udev_rules.html#syntax

- 05 :
	http://www.xml.com/ldd/chapter/book/ch03.html
	https://www.kernel.org/doc/htmldocs/kernel-api/API-snprintf.html
	[Error return code](http://man7.org/linux/man-pages/man3/errno.3.html)

- 07 :
	Need to mount debugfs with ->  mount -t debugfs none /sys/kernel/debug/ then load module
	https://www.kernel.org/doc/Documentation/filesystems/debugfs.txt

- 08 :
	https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/coding-style.rst

- 09 : 
	http://www.science.unitn.it/~fiorella/guidelinux/tlk/node106.html
	https://stackoverflow.com/questions/46591203/get-all-mount-points-in-kernel-module
	https://www.win.tue.nl/~aeb/linux/lk/lk-8.html
	https://elixir.bootlin.com/linux/v4.19.1/source/fs/proc_namespace.c#L308
	http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN708
	https://www.cs.ucsb.edu/~rich/class/cs293b-cloud/papers/lxc-namespace.pdf
