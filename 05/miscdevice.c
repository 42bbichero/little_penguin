// SPDX-License-Identifier: GPL-3.0-Clause

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

/*
 * Send data from device to user
 */
static ssize_t	read_method(struct file *filp, char *ubuf, size_t count,
			    loff_t *f_pos)
{
	int error_count = 0;
	char message[] = "bbichero";

	if (!filp || !ubuf || !f_pos)
		return -EFAULT;
	if (*f_pos >= sizeof(message))
		return 0;
	if (count < sizeof(message))
		return -EINVAL;
	error_count = copy_to_user((void *)ubuf + *f_pos, message,
				   sizeof(message));
	if (error_count == 0) {
		pr_debug("MISC fortytwo: Sent 9 characters to the user\n");
		*f_pos += sizeof(message);
		return sizeof(message);
	}
	pr_debug("MISC fortytwo: Failed to send %d characters to the user\n",
		 error_count);
	return -EFAULT;
}

/*
 * Get data sent by user and compare it with "bbichero" str
 */
static ssize_t	write_method(struct file *filp, const char *ubuf, size_t count,
			     loff_t *f_pos)
{
	int error_count = 0;
	char buf[8];

	if (count == 8) {
		error_count = copy_from_user(buf, (void *)ubuf + *f_pos, count);
		if (error_count == 0 && strncmp(buf, "bbichero", 8) == 0) {
			pr_debug("MISC fortytwo: user wrote %ld chars to device\n",
				 count);
			return count;
		}
	}
	pr_debug("MISC fortytwo: invalid value.\n");
	return -EINVAL;
}

static int	open_method(struct inode *inode, struct file *file)
{
	return 0;
}

static const struct file_operations misc_fops = {
	.owner = THIS_MODULE,
	.read = read_method,
	.write = write_method,
	.open = open_method
};

struct miscdevice	forty_two_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "fortytwo",
	.fops = &misc_fops,
	.nodename = "fortytwo"
};

/*
 * Register the miscdevices struct define
 */
static int __init miscdevice_init(void)
{
	int	ret;

	ret = misc_register(&forty_two_device);
	if (ret) {
		pr_err("Can't register miscdevice");
		return -1;
	}
	pr_debug("miscdevice register\n");

	return 0;
}

static void __exit miscdevice_exit(void)
{
	misc_deregister(&forty_two_device);
	pr_debug("miscdevice unregister\n");
}

module_init(miscdevice_init);
module_exit(miscdevice_exit);

MODULE_AUTHOR("bbichero");
MODULE_DESCRIPTION("little penguin 05");
MODULE_LICENSE("GPL");
