#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/slab.h>

/*
 * Run delay following in receive and
 * tell userspace about it
 */
static int	do_work(int *my_int, int retval)
{
	int x, z;
	int y = *my_int;

	for (x = 0; x < my_int; ++x)
		udelay(10);
	if (y < 10)
		pr_info("We slept a long time!");
	z = x * y;
	return z;
}

static int __init my_init(void)
{
	int x = 10;

	x = do_work(&x, x);
	return x;
}

static void __exit my_exit(void)
{
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("unknown");
MODULE_DESCRIPTION("run delay");
MODULE_LICENSE("GPL");
