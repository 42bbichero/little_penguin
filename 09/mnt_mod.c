#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/seq_file.h>
#include <linux/proc_fs.h>
#include <linux/mount.h>
#include <linux/nsproxy.h>
#include <linux/fs_struct.h>

static int	my_open(struct inode *inodes, struct file *files)
{
	return 0;
}

ssize_t	my_read(struct file *filp, char *ubuf, size_t count, loff_t *f_pos)
{
	struct dentry *curdentry;

	pr_debug("root     %s", current->fs->root.mnt->mnt_root->d_name.name);
	list_for_each_entry(curdentry,
			&current->fs->root.mnt->mnt_root->d_subdirs, d_child) {
		if (curdentry->d_flags & DCACHE_MOUNTED) {
			if (strlen(curdentry->d_name.name) == 3) {
				pr_debug("%s      /%s", curdentry->d_name.name,
							curdentry->d_name.name);
			} else {
				pr_debug("%s     /%s", curdentry->d_name.name,
							curdentry->d_name.name);
			}
		}
	}
	return 0;
}

const struct file_operations proc_fops = {
	.owner = THIS_MODULE,
	.read = my_read,
	.open = my_open
};

/*
 * Register the procdevices struct define
 */
int __init mymount_proc_init(void)
{
	if (!proc_create("mymount", 0644, NULL, &proc_fops)) {
		pr_debug("/proc/%s wasn't created, error.\n", "mymount");
		return -EFAULT;
	}
	pr_debug("/proc/%s created\n", "mymount");
	return 0;
}

void __exit mymount_proc_exit(void)
{
	remove_proc_entry("mymount", NULL);
	pr_debug("/proc/%s removed\n", "mymount");
}

module_init(mymount_proc_init);
module_exit(mymount_proc_exit);

MODULE_AUTHOR("bbichero");
MODULE_DESCRIPTION("little penguin 09");
MODULE_LICENSE("GPL");
