// SPDX-License-Identifier: GPL-3.0-Clause

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init hello_init(void)
{
	pr_debug("Hello world !\n");
	return 0;
}

static void __exit hello_exit(void)
{
	pr_debug("Cleaning up module.\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_AUTHOR("bbichero");
MODULE_DESCRIPTION("Hello world");
MODULE_LICENSE("GPL");
