// SPDX-License-Identifier: GPL-3.0-Clause

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/usb/input.h>
#include <linux/hid.h>

static int	hello_probe(struct usb_interface *intf,
			    const struct usb_device_id *id)
{
	pr_debug("Keyboard hello module: probe function called\n");
	return 0;
}

static void	hello_disconnect(struct usb_interface *intf)
{
	pr_debug("Keyboard hello module: disconnect function called\n");
}

/*
 * register this driver with the USB subsystem
 */
static int	__init hello_init(void)
{
	pr_debug("Keyboard hello module: Hello world !\n");
	return 0;
}

/*
 * deregister this driver with the USB subsystem
 */
static void	__exit hello_exit(void)
{
	pr_debug("Cleaning up module.\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_AUTHOR("bbichero");
MODULE_DESCRIPTION("little penguin ex04");
MODULE_LICENSE("GPL");
