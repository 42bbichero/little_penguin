// SPDX-License-Identifier: GPL-3.0-Clause

#include <linux/module.h>
#include <linux/init.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/jiffies.h>
#include <linux/sched.h>
#include <linux/cred.h>
#include <linux/rwlock_types.h>

static char foo_buffer[PAGE_SIZE];
static rwlock_t myrwlock;

struct t_debugfs {
	struct dentry *root;
	struct dentry *id;
	struct dentry *jiffies;
	struct dentry *foo;
};

static struct t_debugfs dbg;

/*
 *	Send jiffie value to user
 */
static ssize_t	jiffies_read(struct file *filp, char *ubuf, size_t count,
			     loff_t *f_pos)
{
	char buffer[64];
	ssize_t len = 0;

	len = snprintf(buffer, sizeof(buffer), "%llu\n", get_jiffies_64());
	return simple_read_from_buffer(ubuf, count, f_pos, buffer, len);
}

static const struct file_operations jiffies_fops = {
	.owner = THIS_MODULE,
	.read = jiffies_read
};

/*
 *	Send data from device to user
 */
static ssize_t	foo_read(struct file *filp, char *ubuf, size_t count,
			 loff_t *f_pos)
{
	ssize_t	ret;

	read_lock(&myrwlock);
	ret = simple_read_from_buffer(ubuf, count, f_pos, foo_buffer,
				      PAGE_SIZE);
	read_unlock(&myrwlock);
	return ret;
}

/*
 *	Get data sent by user and compare it with "bbichero" str
 */
static ssize_t foo_write(struct file *filp, const char *ubuf, size_t count,
			 loff_t *f_pos)
{
	ssize_t ret;

	memset(foo_buffer, 0, PAGE_SIZE);
	if (*f_pos >= PAGE_SIZE)
		return -EINVAL;
	write_lock(&myrwlock);
	ret = simple_write_to_buffer(foo_buffer, sizeof(foo_buffer), f_pos,
				     ubuf, count);
	if (ret < 0)
		return 0;
	foo_buffer[ret] = '\0';
	write_unlock(&myrwlock);
	return ret;
}

static const struct file_operations foo_fops = {
	.owner = THIS_MODULE,
	.read = foo_read,
	.write = foo_write
};

/*
 * Send data from device to user
 */
static ssize_t	id_read(struct file *filp, char *ubuf, size_t count,
			loff_t *f_pos)
{
	int error_count = 0;
	char message[] = "bbichero";

	if (!filp || !ubuf || !f_pos)
		return -EFAULT;
	if (*f_pos >= sizeof(message))
		return 0;
	if (count < sizeof(message))
		return -EINVAL;
	error_count = copy_to_user((void *)ubuf + *f_pos, message,
				   sizeof(message));
	if (error_count == 0) {
		pr_debug("MISC fortytwo: Sent 9 characters to the user\n");
		*f_pos += sizeof(message);
		return sizeof(message);
	}
	pr_debug("MISC fortytwo: Failed to send %d characters to the user\n",
		 error_count);
	return -EFAULT;
}

/*
 * Get data sent by user and compare it with "bbichero" str
 */
static ssize_t	id_write(struct file *filp, const char *ubuf, size_t count,
			 loff_t *f_pos)
{
	int error_count = 0;
	char buf[8];

	if (count == 8) {
		error_count = copy_from_user(buf, (void *)ubuf + *f_pos, count);
		if (error_count == 0 && strncmp(buf, "bbichero", 8) == 0) {
			pr_debug("MISC fortytwo: user wrote %ld chars to device\n",
				 count);
			return count;
		}
	}
	pr_debug("MISC fortytwo: invalid value.\n");
	return -EINVAL;
}

static const struct file_operations id_fops = {
	.owner = THIS_MODULE,
	.read = id_read,
	.write = id_write
};

int	create_root(char *dirname)
{
	dbg.root = debugfs_create_dir(dirname, NULL);
	if (dbg.root == ERR_PTR(-ENODEV))
		return -ENODEV;
	return 0;
}

int	create_id_file(char *filename, int perm)
{
	dbg.id = debugfs_create_file(filename, perm, dbg.root, NULL, &id_fops);
	if (dbg.id == ERR_PTR(-ENODEV))
		return -ENODEV;
	return 0;
}

int	create_foo_file(char *filename, int perm)
{
	dbg.foo = debugfs_create_file(filename, perm, dbg.root, NULL,
				      &foo_fops);
	if (dbg.id == ERR_PTR(-ENODEV))
		return -ENODEV;
	return 0;
}

int	create_jiffies_file(char *filename, int perm)
{
	dbg.id = debugfs_create_file(filename, perm, dbg.root, NULL,
				     &jiffies_fops);
	if (dbg.id == ERR_PTR(-ENODEV))
		return -ENODEV;
	return 0;
}

static int __init debugfs_init(void)
{
	int err;

	err = create_root("fortytwo");
	if (err)
		pr_debug("Error, can't create directory fortytwo\n");
	err = create_id_file("id", 0666);
	if (err)
		pr_debug("Error, can't create file id\n");
	err = create_jiffies_file("jiffies", 0444);
	if (err)
		pr_debug("Error, can't create file jiffies\n");
	err = create_foo_file("foo", 0644);
	if (err)
		pr_debug("Error, can't create file foo\n");
	pr_debug("Create one dir and 2 files\n");
	return 0;
}

static void __exit debugfs_exit(void)
{
	pr_debug("remove fortytwo directory.\n");
	debugfs_remove_recursive(dbg.root);
}

MODULE_AUTHOR("bbichero");
MODULE_DESCRIPTION("little penguin 07");
MODULE_LICENSE("GPL");

module_init(debugfs_init);
module_exit(debugfs_exit);
